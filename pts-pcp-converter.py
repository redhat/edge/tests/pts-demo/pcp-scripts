#!/usr/bin/python

import os
import sys
import json
import hashlib
from datetime import datetime

def main():
    #Step 1: 
    #input args, 1 benchmark-wrapper.archive file 2 pcp.json file

    archive_file=sys.argv[1]
    pcp_file=sys.argv[2]

    print(archive_file, pcp_file)

    #Step 2: 
    # open/read archive file
    # store a dict of uuid, user, name, cluser, sample, and test_config.
    # with date that each occured, this will allow us to label pcp data 
    # with more specifc metadata for sample/test_config changes

    metadata = {}
    sample_list = []
    with open(archive_file) as fp:
        Lines = fp.readlines()
        for line in Lines:
            data = json.loads(line)
            sample_metadata = data["_source"]
            metadata["uuid"] = sample_metadata["uuid"]
            metadata["user"] = sample_metadata["user"]
            metadata["clustername"] = sample_metadata["clustername"]
            metadata["run_id"] = sample_metadata["run_id"]
            sample = {}
            sample["sample"] = sample_metadata["sample"]
            sample["test_config"] = sample_metadata["test_config"]
            sample["datetime"] = sample_metadata["date"]

            if sample not in sample_list:
                sample_list.append(sample)

    #print(json.dumps(metadata, indent=4))

    #Step 3: 
    #ingest pcp file
    #flatten out structure - currently network is a list need to convert to a dict
    #add metadate to pcp data, check time for appropriate sample/test_config

    dfile = open(pcp_file, "r")
    pcp_datafile = json.loads(dfile.read())


    for  host in pcp_datafile["@pcp"]["@hosts"]:
        #print(host["@host"])
        for metric in host["@metrics"]:

            # get pcp data point timestamp
            dtime = datetime.strptime(metric["@timestamp"], "%Y-%m-%d %H:%M:%S")
            # set the initial sample data to sample 1
            sample_data = sample_list[0] 
            # loop through the list of samples taken from benchmark-wrapper
            # if the pcp data point is greater than the sample time merge data
            for sample in sample_list:
                stime = datetime.strptime(sample["datetime"], "%Y-%m-%dT%H:%M:%S.%fZ")
                #print("d:%s - s:%s" % (dtime, stime)) 
                if dtime >= stime:
                    sample_data = sample
            metric.update(metadata)
            metric["host"] = host["@host"]
            metric.update(sample_data)

    #Step 4: 
    #write out pcp archive file compatabile with run_snafu indexing. 

            #print(json.dumps(metric, indent=4))
            es_valid_document = {"_index": "pcp_data", "_op_type": "create", "_source": metric, "_id": ""}
            es_valid_document["_id"] = hashlib.sha256(str(metric).encode()).hexdigest()
            print(json.dumps(es_valid_document, indent=4))
            write_to_archive_file(es_valid_document)

def write_to_archive_file(es_friendly_documment):

    #  assumes that all documents have the same structure
    user = es_friendly_documment["_source"]["user"]
    clustername = es_friendly_documment["_source"]["clustername"]
    uuid = es_friendly_documment["_source"]["uuid"]
    #  create archive file as user_clustername_uuid.archive in cwd
    archive_filename = "PCP_" + user + "_" + clustername + "_" + uuid + ".archive"

    #  Will write each es friendly document on 1 line, this makes re-indexing easier later
    with open(archive_filename, "a") as f:
        json.dump(es_friendly_documment, f)
        f.write(os.linesep)

if __name__ == "__main__":
    sys.exit(main())
