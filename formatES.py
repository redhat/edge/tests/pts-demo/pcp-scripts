from datetime import datetime, timedelta
import os
import uuid
import hashlib
import json
import csv

es_index = {"_index": "pcp-ffi", "_op_type": "create", "_source": {}, "_id": "", "run_id": "NA"}

#Helper function for parsing csv file data
def conv(data):
    try:
        data=float(data)
    except ValueError:
        data=None
    return data

#Helper function for assigning appropriate prefix notation to byte data
def get_bytes(size, suffix):
    size = float(size)
    suffix = suffix.lower()

    if suffix == 'kb' or suffix == 'kib':
        return size * 1024
    elif suffix == 'mb' or suffix == 'mib':
        return size * 1024**2
    elif suffix == 'gb' or suffix == 'gib':
        return size * 1024**3


#Parse a given file
def parse(filepath, result_summary):

    #Name of test for outfile path and ES purposes
    test = os.path.basename(filepath).split('.')[0]

    #Grab safe/nonsafe, stage, and iteration from test
    app_type = test.split('_')[0]
    stage = test.split('_')[2]
    iteration = test.split('_')[3]
    if iteration.isalpha():
        iteration = 0

    #Specify app, stage, iteration, and metric for ES
    result_summary["test_config"] = {"app": app_type, "stage": stage, "iteration": iteration}
    #PCP data file
    outfile = test + '_PCP.archive'

    #Bring data into memory
    with open(filepath, newline='') as f_csv:
        lines = csv.DictReader(f_csv)
        #Get headers for later
        metrics = lines.fieldnames


        #Parse data and write to archive file
        with open(outfile, 'w') as f:

            #For each line in data file
            for line in lines:

                #Create ES result index with test results and datetime as time stamp
                result_summary["date"] = line[metrics[0]]

                #For each column in csv file
                for i in range(1, len(line)):
                    #Parse metric data
                    result_summary["test_results"].append({metrics[i].split('-')[0]: conv(line[metrics[i]])})

                #Format ES output
                output = es_index
                output["_source"] = result_summary
                output["_id"] = hashlib.sha256(str(result_summary).encode()).hexdigest()

                #write ES result to .archive file
                f.write(json.dumps(output)+'\n')


def main(argv):
    #Format ES result_summary json
    result_uuid = os.getenv("uuid", str(uuid.uuid4()))
    result_summary = {
        "uuid": result_uuid,
        #TODO pull user from machine
        "user": "sberg",
        #TODO pull machine name
        "clustername": "1-aws_a1.xlarge-baremetal-centos8",
        "date": 0,
        "sample": 1,
        "test_config": {},
        "test_results": [],
        "run_id": "NA"
    }

    #Need input file arg
    if len(argv) != 2:
        raise SystemExit(f'Usage: {argv[0]} ' 'formatES <input file>')

    #take arg as filepath
    filepath = argv[1]

    parse(filepath, result_summary)

if __name__ == '__main__':
    import sys
    main(sys.argv)
